/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.orhonit.publicity;

import com.orhonit.publicity.datasource.config.DynamicDataSourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class},scanBasePackages = "com.orhonit")
//@Import({DynamicDataSourceConfig.class})
//(scanBasePackages = "com.orhonit")
//(exclude = DataSourceAutoConfiguration.class)
public class PublicityApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublicityApplication.class, args);
	}

}