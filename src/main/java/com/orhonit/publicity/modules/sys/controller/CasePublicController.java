package com.orhonit.publicity.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.R;
import com.orhonit.publicity.modules.sys.entity.CasePublicEntity;
import com.orhonit.publicity.modules.sys.service.CasePublicService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



/**
 * 案件公示
 */
@RestController
@RequestMapping("sys/casePublic")
public class CasePublicController {
    @Autowired
    private CasePublicService casePublicService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:casePublic:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = casePublicService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{caseId}")
    @RequiresPermissions("sys:casePublic:info")
    public R info(@PathVariable("caseId") Long caseId){
		CasePublicEntity casePublic = casePublicService.getById(caseId);

        return R.ok().put("casePublic", casePublic);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:casePublic:save")
    public R save(@RequestBody CasePublicEntity casePublic){
		casePublicService.save(casePublic);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:casePublic:update")
    public R update(@RequestBody CasePublicEntity casePublic){
		casePublicService.updateById(casePublic);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:casePublic:delete")
    public R delete(@RequestBody Long[] caseIds){
		casePublicService.removeByIds(Arrays.asList(caseIds));

        return R.ok();
    }

}
