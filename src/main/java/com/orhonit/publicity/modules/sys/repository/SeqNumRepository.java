package com.orhonit.publicity.modules.sys.repository;

import com.orhonit.publicity.modules.sys.model.SeqSerializerNum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeqNumRepository extends JpaRepository<SeqSerializerNum,String> {

}
