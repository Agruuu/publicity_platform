package com.orhonit.publicity.modules.sys.dao;

import com.orhonit.publicity.modules.sys.dto.AreaDeptDTO;
import com.orhonit.publicity.modules.sys.dto.DeptDTO;
import com.orhonit.publicity.modules.sys.entity.DeptEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 执法主体
 */
@Mapper
public interface DeptDao extends BaseMapper<DeptEntity> {

    /**
     * 获取该主体能管理的所有主体列表
     */
    List<DeptDTO> getDeptByManageDeptIdList(@Param("params") Map<String, Object> params);

    /**
     * 获取执法主体列表
     */
    List<DeptDTO> getDeptList(@Param("params") Map<String, Object> params);


    /**
     * 各辖区主体数量统计
     */
    List<AreaDeptDTO> areaDeptList();

    /**
     * 各辖区主体数量和性质统计
     */
    List<AreaDeptDTO> areaDeptProList(@Param("params") Map<String, Object> params);

    /**
     * 获取执法主体详细列表
     */
    List<AreaDeptDTO> getDeptAllList(@Param("params") Map<String, Object> params);

    /**
     * 根据主体Id查询本主体及下级主体列表
     */
    List<DeptDTO> getDeptListByDeptId(@Param("params") Map<String, Object> params);

    /**
     * 执法人员总数
     */
    String getPersonCount(@Param("params") Map<String, Object> params);

    /**
     * 执法人员总数
     */
    String getPersonCountByDeptId(@Param("params") Map<String, Object> params);

    /**
     * 委托机构总数
     */
    String getDeptAgentCountByDeptId(@Param("params") Map<String, Object> params);

//    /**
//     * 案件数量
//     */
//    String getcaseCountByDeptId(@Param("params") Map<String, Object> params);

    /**
     * 根据区域Id和区域类型查询主体
     */
    List<DeptDTO>  selDeptByDeptId(@Param("params") Map<String, Object> params);

    /**
     * 根据主体ID查询主体信息
     * */
    DeptDTO findDeptInfoById(@Param("deptId")String deptId);

    @Select("SELECT * FROM dept WHERE area_id = #{id} and if_effect='1' and del_flag ='0'")
    List<DeptDTO> deptTreeByAreaId(String id);

    @Select("SELECT * FROM dept where if_effect='1' and del_flag ='0'")
    List<DeptDTO> deptTreeAll();

    @Select("SELECT * FROM dept WHERE  if_effect='1' and del_flag ='0' and FIND_IN_SET(id,getBaseDept(#{id}))")
    List<DeptDTO> deptTreeByDeptId(String id);
}
