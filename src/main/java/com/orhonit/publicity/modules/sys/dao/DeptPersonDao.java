package com.orhonit.publicity.modules.sys.dao;

import com.orhonit.publicity.modules.sys.dto.*;
import com.orhonit.publicity.modules.sys.entity.DeptPersonEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 主体人员表
 */
@Mapper
public interface DeptPersonDao extends BaseMapper<DeptPersonEntity> {

    /**
     * 根据参数获取执法人员列表信息
     */
    List<DeptPersonDTO> getPersonListByParam(@Param("params") Map<String, Object> params);

    /**
     * 查询人员信息
     */
    DeptPersonDTO findPersonInfo(String id);

    /**
     * 查询用户信息
     */
    DeptPersonDTO findUserInfo(String id);

    /**
     * 根据执法人员ID 查询用户ID
     */
    DeptPersonDTO findPersonId(String id);

    /**
     * 执法人员查询
     */
    List<DeptPersonDTO> getPerDTO(@Param("ping") Map<String, Object> ping);

    /**
     * 执法人员详情
     */
    List<DeptPersonDTO> getSelectByPersonId(@Param("personIdMp") Map<String, Object> personIdMp);

    /**
     * 各个区域执法人员执法类型数量统计
     */
    List<PersonInAreaCountDTO> getPeAndArSelect();

    /**
     * PC各个区域执法人员数量统计
     */
    List<PersonInAreaCountDTO> getPeAndAr();

    /**
     * PS
     * 一个地区下的每个部门的人员统计
     */
    List<PersonInDeptCountDTO> getPeAndDepByAre(@Param("pADBMap") Map<String, Object> pADBMap);

    /**
     * ps
     * 执法人员查询（条形图用）
     */
    List<LawPersonDTO> getSelectLaw();
    /**
     * ps
     * 监督人员查询(条形图用)
     * @return
     */
    List<LawPersonDTO> getSelectSup();
    /**
     * ps
     * 执法人员统计
     * @return
     */
    List<LawPersonDTO> getAllLaw();

    /**
     * 查询审核审批负责人
     * */
    DeptPersonDTO findPersonName(@Param("params") Map<String, Object> params);

    /**
     * 根据参数获取执法人员列表信息
     */
    List<DeptPersonDTO> getPersonListByDeptId(String deptId);

    /**
     * 根据区域id查询执法人员详细信息
     */
    List<DeptPersonDTO> getSelectByAreaId(DeptPersonDTO selectPersonByAreaIdDTO);

    /**
     * PS PC根据区域id查询执法人员信息
     */
    List<DeptPersonDTO> getByAreaId(DeptPersonDTO selectPersonByAreaIdDTO);

    /**
     * 查询执法人员列表总条数
     */
    public int getMessageNum(String areaName);

    /**
     * PS PC执法人员列表查询总数
     */
    public int getMessagNum(String areaName);

    /**
     * 查询执法人员列表总条数
     */
    public int getMesNum(String deptId);

    /**
     * 根据deptId查询执法人员详细信息
     */
    List<DeptPersonDTO> getSelectBydeptIdPC(DeptPersonDTO selectPersonByAreaIdDTO);

    /**
     * 根据deptId查询执法人员详细信息
     */
    List<DeptPersonDTO> getSelectBydeptId(@Param("param") Map<String, Object> param);

    /**
     * 根据人员id查询执法人员详细
     */
    List<DeptPersonDTO> getSelectPersonById(@Param("mapGetSelectPersonById") Map<String, Object> mapGetSelectPersonById);

    List<DeptPersonDTO> getAppPersonListByParam(@Param("param") Map<String, Object> param);

    List<DeptPersonDTO> getPersonRole(String id);

    DeptPersonDTO getPersonInfoByPersonId(@Param("personId") String personId);


//    /**
//     * 查询案件主执法人员
//     */
////    ZfrDTO getZzfrInfo(String id);
//
//    /**
//     * 查询案件外勤人员
//     */
////    ZfrDTO getWqrInfo(String id);
//
//    /**
//     * 查询按键主执法人员
//     */
////    List<ZfrDTO> getFzfrInfo(String id);

//    /**
//     * 根据区域id查询执法人员详细信息
//     */
//    List<SelectPersonByAreaIdDTO> getSelectByAreaId(SelectPersonByAreaIdDTO selectPersonByAreaIdDTO);
//
//    /**
//     * PS PC根据区域id查询执法人员信息
//     */
//    List<SelectPersonByAreaIdDTO> getByAreaId(SelectPersonByAreaIdDTO selectPersonByAreaIdDTO);
//
//    /**
//     * 根据deptId查询执法人员详细信息
//     */
//    List<SelectPersonByAreaIdDTO> getSelectBydeptIdPC(SelectPersonByAreaIdDTO selectPersonByAreaIdDTO);
//
//    /**
//     * 根据deptId查询执法人员详细信息
//     */
//    List<SelectPersonByAreaIdDTO> getSelectBydeptId(@Param("param") Map<String, Object> param);
}
