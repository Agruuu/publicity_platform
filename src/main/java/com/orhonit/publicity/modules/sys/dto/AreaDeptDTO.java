package com.orhonit.publicity.modules.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
//@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class AreaDeptDTO {

    private String id;

    /**
     * 区划id
     */
    private String areaId;

    /**
     * 区划名称
     */
    private String areaName;

    /**
     * 主体 性质：1授权执法单位 2委托执法单位 3行政执法机关
     */
    private String deptProperty;

    /**
     * 主体名称
     */
    private String deptName;

    /**
     * 主体名称简称
     */
    private String shortName;

    /**
     * 主体编号
     */
    private String code;

    /**
     * 主体地址
     */
    private String address;

    /**
     * 区划下主体数量吧
     */
    private String count;
}
