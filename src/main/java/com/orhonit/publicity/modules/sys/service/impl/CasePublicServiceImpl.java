package com.orhonit.publicity.modules.sys.service.impl;

import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.Query;
import com.orhonit.publicity.modules.sys.dao.CasePublicDao;
import com.orhonit.publicity.modules.sys.entity.CasePublicEntity;
import com.orhonit.publicity.modules.sys.service.CasePublicService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("casePublicService")
public class CasePublicServiceImpl extends ServiceImpl<CasePublicDao, CasePublicEntity> implements CasePublicService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CasePublicEntity> page = this.page(
                new Query<CasePublicEntity>().getPage(params),
                new QueryWrapper<CasePublicEntity>()
        );

        return new PageUtils(page);
    }

}