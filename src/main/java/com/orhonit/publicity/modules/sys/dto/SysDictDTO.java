package com.orhonit.publicity.modules.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统字典
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SysDictDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private String id;
	/**
	 * 大类值
	 */
	private String typeValue;
	/**
	 * 大类描述
	 */
	private String typeDesc;
	/**
	 * 键值
	 */
	private String enumValue;
	/**
	 * 描述
	 */
	private String enumDesc;
	/**
	 * 创建时间
	 */
	private Date createdDate;
	/**
	 * 更新时间
	 */
	private Date lastUpdate;
	/**
	 * 语言
	 */
	private String lang;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 蒙文描述
	 */
	private String mglEnumDesc;
	/**
	 * 蒙文大类描述
	 */
	private String mglTypeDesc;

}
