package com.orhonit.publicity.modules.sys.dao;

import com.orhonit.publicity.modules.sys.entity.AreaEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 行政区划(区域)表
 */
@Mapper
public interface AreaDao extends BaseMapper<AreaEntity> {
	
}
