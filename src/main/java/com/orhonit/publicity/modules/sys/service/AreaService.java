package com.orhonit.publicity.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.modules.sys.entity.AreaEntity;

import java.util.Map;

/**
 * 行政区划(区域)表
 */
public interface AreaService extends IService<AreaEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

