package com.orhonit.publicity.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 案件公示
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@TableName("case_public")
public class CasePublicEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@Id
	private Long caseId;
	/**
	 * 行政相对人名称
	 */
	private String casePartyName;
	/**
	 * 行政相对人类型
	 */
	private String casePartyType;
	/**
	 * 证件类型
	 */
	private String caseCertType;
	/**
	 * 证件号码
	 */
	private String caseCertNum;
	/**
	 * 执法主体id
	 */
	private String caseDeptId;
	/**
	 * 区划Id
	 */
	private String caseAreaId;
	/**
	 * 主体人员id
	 */
	private String caseDeptPersonId;
	/**
	 * 案件名称
	 */
	private String caseName;
	/**
	 * 决定书名称
	 */
	private String caseDecisionName;
	/**
	 * 决定书文号
	 */
	private String caseDecisionTitanic;
	/**
	 * 罚款金额
	 */
	private String caseFines;
	/**
	 * 处罚决定日期
	 */
	private Date caseDateOfPunishmentDecision;
	/**
	 * 处罚类型
	 */
	private String casePunishmentType;
	/**
	 * 违反的法律法规
	 */
	private String caseViolationOfLaw;
	/**
	 * 违法事实
	 */
	private String caseIllegalFacts;
	/**
	 * 处罚依据
	 */
	private String casePunishmentGist;
	/**
	 * 是否公示  0： 否  1：是
	 */
	private String isPs;

}
