package com.orhonit.publicity.modules.sys.controller;

import com.orhonit.publicity.common.datatables.TableRequest;
import com.orhonit.publicity.common.datatables.TableRequestHandler;
import com.orhonit.publicity.common.datatables.TableResponse;
import com.orhonit.publicity.modules.sys.dao.LepDao;
import com.orhonit.publicity.modules.sys.dao.LtcAttDao;
import com.orhonit.publicity.modules.sys.model.Lepeson;
import com.orhonit.publicity.modules.sys.model.LtcAtt;
import com.orhonit.publicity.modules.sys.model.User;
import com.orhonit.publicity.modules.sys.utils.UserUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 执法人员列表控制器
 *
 */
@RestController
@RequestMapping("/LepLists")
public class LepController {

	@Autowired
	private LepDao lepDao;
	
	@Autowired
	private LtcAttDao ltcAttDao;

	@GetMapping
	@ApiOperation(value = "执法人列表")
	public TableResponse<Lepeson> list(TableRequest request) {
		User user = UserUtil.getCurrentUser();
		LtcAtt ltcAtt = ltcAttDao.getLtc(user.getDeptId());
		request.getParams().put("dept_id", user.getDeptId());
		if(ltcAtt != null && ltcAtt.getDept_property() == 3 && ltcAtt.getLaw_type().equals("2")){
			//法制办
			//如果是市本级的法制办则显示所有
			if(!user.getAreaId().equals("15")){
				request.getParams().put("lx_type", 1);
			}
		}else if(user.getUsername().equals("admin")){
			//管理员不需要任何操作
		}
		else{
			//委办局
			request.getParams().put("lx_type", 2);
			request.getParams().put("deptIds", lepDao.execFunction(null, user.getDeptId()));
		}
		return TableRequestHandler.<Lepeson> builder().countHandler(new TableRequestHandler.CountHandler() {

			@Override
			public int count(TableRequest request) {
				int result=lepDao.count(request.getParams());
				return result;
			}
		}).listHandler(new TableRequestHandler.ListHandler<Lepeson>() {

			@Override
			public List<Lepeson> list(TableRequest request) {
				
				List<Lepeson> lists=lepDao.list(request.getParams(), request.getStart(), request.getLength());
				return lists;
			}
		}).build().handle(request);
	}

}
