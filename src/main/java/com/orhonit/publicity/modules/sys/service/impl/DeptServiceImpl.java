package com.orhonit.publicity.modules.sys.service.impl;

import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.Query;
import com.orhonit.publicity.modules.sys.dao.DeptDao;
import com.orhonit.publicity.modules.sys.dto.AreaDeptDTO;
import com.orhonit.publicity.modules.sys.dto.DeptDTO;
import com.orhonit.publicity.modules.sys.entity.DeptEntity;
import com.orhonit.publicity.modules.sys.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("deptService")
public class DeptServiceImpl extends ServiceImpl<DeptDao, DeptEntity> implements DeptService {

    @Autowired
    private DeptDao deptDao;

    /**
     * 获取该主体能管理的所有主体列表
     */
    @Override
    public List<DeptDTO> getDeptByManageDeptIdList(Map<String, Object> paramMap) {
        return this.deptDao.getDeptByManageDeptIdList(paramMap);
    }

    @Override
    public List<DeptDTO> getDeptList(Map<String, Object> paramMap) {
        return this.deptDao.getDeptList(paramMap);
    }

    @Override
    public List<AreaDeptDTO> areaDeptList() {
        return this.deptDao.areaDeptList();
    }

    @Override
    public List<AreaDeptDTO> areaDeptProList(Map<String, Object> paramMap) {
        return this.deptDao.areaDeptProList(paramMap);
    }

    @Override
    public List<AreaDeptDTO> getDeptAllList(Map<String, Object> paramMap) {
        return this.deptDao.getDeptAllList(paramMap);
    }
    @Override
    public List<DeptDTO> getDeptListByDeptId(Map<String, Object> paramMap) {
        return this.deptDao.getDeptListByDeptId(paramMap);
    }
    @Override
    public DeptDTO getCountByDeptId(Map<String, Object> paramMap) {
        String personCount =deptDao.getPersonCountByDeptId(paramMap);
        String deptAgentCount = deptDao.getDeptAgentCountByDeptId(paramMap);
        // TODO
        // 案件公示
        String caseCount = "0";
        DeptDTO deptDTO = new DeptDTO();
        deptDTO.setPersonCount(personCount);
        deptDTO.setDeptAgentCount(deptAgentCount);
        deptDTO.setCaseCount(caseCount);
        return deptDTO;
//        String lawCount = deptDao.getLawCountByDeptId(paramMap);
//        String potenceCount = deptDao.getPotenceCountByDeptId(paramMap);
//        deptDTO.setLawCount(lawCount);
//        deptDTO.setPotenceCount(potenceCount);
//        MultipartProperties
    }
    @Override
    public List<DeptDTO> selDeptByDeptId(Map<String, Object> paramMap) {
        return this.deptDao.selDeptByDeptId(paramMap);
    }

    @Override
    public DeptDTO findDeptInfoById(String deptId) {
        return deptDao.findDeptInfoById(deptId);
    }

    @Override
    public List<DeptDTO> deptTreeByAreaId(String deptId) {
        return deptDao.deptTreeByAreaId(deptId);
    }

    @Override
    public List<DeptDTO> deptTreeAll() {
        return deptDao.deptTreeAll();
    }

    @Override
    public List<DeptDTO> deptTreeByDeptId(String deptId) {
        return deptDao.deptTreeByDeptId(deptId);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DeptEntity> page = this.page(
                new Query<DeptEntity>().getPage(params),
                new QueryWrapper<DeptEntity>()
        );

        return new PageUtils(page);
    }

}