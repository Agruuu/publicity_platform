package com.orhonit.publicity.modules.sys.dao;

import com.orhonit.publicity.modules.sys.dto.CasePublicDTO;
import com.orhonit.publicity.modules.sys.entity.CasePublicEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 案件公示
 */
@Mapper
public interface CasePublicDao extends BaseMapper<CasePublicEntity> {

    /**
     * 根据参数获取执法人员列表信息
     */
//    List<DeptPersonDTO> getPersonListByParam(@Param("params") Map<String, Object> params);

    List<CasePublicDTO> getCasePublicByProperties(@Param("params") Map<String, Object> params);

    List<CasePublicDTO> getCasePublicByArea(@Param("params") Map<String, Object> params);

    List<CasePublicDTO> getCasePublicByDeptId(@Param("params") Map<String, Object> params);
}
