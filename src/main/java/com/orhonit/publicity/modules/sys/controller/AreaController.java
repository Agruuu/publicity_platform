package com.orhonit.publicity.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.R;
import com.orhonit.publicity.modules.sys.entity.AreaEntity;
import com.orhonit.publicity.modules.sys.service.AreaService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



/**
 * 行政区划(区域)表
 */
@RestController
@RequestMapping("sys/area")
public class AreaController {
    @Autowired
    private AreaService areaService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:area:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = areaService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:area:info")
    public R info(@PathVariable("id") Integer id){
		AreaEntity area = areaService.getById(id);

        return R.ok().put("area", area);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:area:save")
    public R save(@RequestBody AreaEntity area){
		areaService.save(area);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:area:update")
    public R update(@RequestBody AreaEntity area){
		areaService.updateById(area);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:area:delete")
    public R delete(@RequestBody Integer[] ids){
		areaService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
