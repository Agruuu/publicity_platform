package com.orhonit.publicity.modules.sys.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orhonit.publicity.common.model.PageEntity;
import com.orhonit.publicity.modules.sys.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 主体人员表
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DeptPersonDTO extends PageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private String id;
	/**
	 * 人员编号
	 */
	private String code;
	/**
	 * 人员姓名
	 */
	private String name;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 民族
	 */
	private String nation;
	/**
	 * 电话
	 */
	private String tel;
	/**
	 * 政治面貌
	 */
	private String political;
	/**
	 * 出生日期
	 */
	private Date birthday;
	/**
	 * 文化程度
	 */
	private String edu;
	/**
	 * 身份证号码
	 */
	private String cardNum;
	/**
	 * 照片
	 */
	private String picture;
	/**
	 * 职务类型
	 */
	private String duty;
	/**
	 * 执法主体
	 */
	private String deptId;
	/**
	 * 执法主体名称
	 */
	private String deptName;
	/**
	 * 区划id
	 */
	private String areaId;
	/**
	 * 区划名称
	 */
	private String areaName;
	/**
	 * 执法证号
	 */
	private String certNum;
	/**
	 * 执法范围
	 */
	private String lawArea;
	/**
	 * 执法证件类型
	 */
	private String certType;
	/**
	 * 执法类别   防汛；水利；水资源',
	 */
	private String certCategory;
	/**
	 * 执法证发证机关
	 */
	private String certAuth;
	/**
	 * 发证时间
	 */
	private Date certTime;
	/**
	 * 证件有效期
	 */
	private Date certTerm;
	/**
	 * 执法性质  行政机关；授权单位',
	 */
	private String lawPro;
	/**
	 * 执法类型  执法 监督
	 */
	private String lawType;
	/**
	 * 是否有效
	 */
	private String ifEffect;
	/**
	 * 删除标志
	 */
	private String delFlag;
	/**
	 * 录入时间 其它系统录入时间
	 */
	private Date otherDate;
	/**
	 * 创建人登录名称
	 */
	private String createBy;
	/**
	 * 创建人名称
	 */
	private String createName;
	/**
	 * 创建日期
	 */
	private Date createDate;
	/**
	 * 更新人登录名称
	 */
	private String updateBy;
	/**
	 * 更新人名称
	 */
	private String updateName;
	/**
	 * 更新日期
	 */
	private Date updateDate;
	/**
	 * 人员姓名(蒙文)
	 */
	private String mglName;
	/**
	 * 创建人名称(蒙文)
	 */
	private String mglCreateName;
	/**
	 * 更新人名称(蒙文)
	 */
	private String mglUpdateName;
	/**
	 * 蒙文执法区域
	 */
	private String mglLawarea;
	/**
	 * 发证机关蒙文
	 */
	private String mglCertAuth;

	@JsonIgnore
	private Integer isEffect;

	private List<Role> roles;

	private Integer roleId;

	private Long userId;
}
