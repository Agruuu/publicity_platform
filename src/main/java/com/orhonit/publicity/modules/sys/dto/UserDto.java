package com.orhonit.publicity.modules.sys.dto;

import com.orhonit.publicity.modules.sys.model.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDto extends User {

	private static final long serialVersionUID = -184009306207076712L;

	private List<Long> roleIds;

}
