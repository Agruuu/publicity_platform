package com.orhonit.publicity.modules.sys.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class User {// extends BaseEntity<Long> {

	private static final long serialVersionUID = -6525908145032868837L;

	private Long userId;

	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 盐
	 */
	@JsonIgnore
	private String salt;
	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 联系电话
	 */
	private String mobile;

	/**
	 * 状态  0：禁用   1：正常
	 */
	private Integer status;
	/**
	 * 主体ID
	 */
	private String deptId;
	/**
	 * 主体名称
	 */
	private String deptName;
	/**
	 * 区划id
	 */
	private String areaId;

	/**
	 * 区划名称
	 */
	private String areaName;

	/**
	 * 管理主体Id
	 */
	private String manageDeptId;

	/**
	 * 创建者Id
	 */
	private Long createUserId;



	public interface Status {
		int DISABLED = 0;
		int VALID = 1;
		int LOCKED = 2;
	}
	
	/**
	 * 判断是否超级管理员
	 * @return
	 */
	public boolean isAdmin(){
		boolean isAdmin=false;
		if("admin".equals(username)){
			isAdmin=true;
		}
		return isAdmin;
	}

//	private int roleId;
//	private String roleName;
	//	private String person_id;
	//	@JsonFormat(pattern = "yyyy-MM-dd")
//	private Date birthday;
//	private Integer sex;
	//	private String telephone;
	//	private String headImgUrl;
}
