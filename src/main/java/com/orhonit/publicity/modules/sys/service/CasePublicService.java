package com.orhonit.publicity.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.modules.sys.entity.CasePublicEntity;

import java.util.Map;

/**
 * 案件公示
 */
public interface CasePublicService extends IService<CasePublicEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

