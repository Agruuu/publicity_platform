package com.orhonit.publicity.modules.sys.service.impl;

import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.Query;
import com.orhonit.publicity.modules.sys.dao.DeptPersonDao;
import com.orhonit.publicity.modules.sys.dto.DeptPersonDTO;
import com.orhonit.publicity.modules.sys.dto.LawPersonDTO;
import com.orhonit.publicity.modules.sys.dto.PersonInAreaCountDTO;
import com.orhonit.publicity.modules.sys.dto.PersonInDeptCountDTO;
import com.orhonit.publicity.modules.sys.entity.DeptPersonEntity;
import com.orhonit.publicity.modules.sys.service.DeptPersonService;
import com.orhonit.publicity.modules.sys.utils.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("deptPersonService")
public class DeptPersonServiceImpl extends ServiceImpl<DeptPersonDao, DeptPersonEntity> implements DeptPersonService {

    @Autowired
    private DeptPersonDao personDao;

//    @Autowired
//    private SmsRepository smsRepository;

    @Override
    public List<DeptPersonDTO> getPersonListByParam(Map<String, Object> paramMap) {
        return personDao.getPersonListByParam(paramMap);
    }

    /**
     * 执法人员查询
     */
    @Override
    public List<DeptPersonDTO> getPerDTO(Map<String, Object> ping) {

        return this.personDao.getPerDTO(ping);
    }

    /**
     * 执法人员详细
     */
    @Override
    public List<DeptPersonDTO> getSelectByPersonId(Map<String, Object> personIdMp) {

        return this.personDao.getSelectByPersonId(personIdMp);
    }

    /**
     * 各个区域执法人员执法类型数量统计
     */
    @Override
    public List<PersonInAreaCountDTO> getPeAndArSelect() {

        return this.personDao.getPeAndArSelect();
    }
    /**
     * PC各个区域执法人员数量统计
     */
    @Override
    public List<PersonInAreaCountDTO> getPeAndAr() {

        return this.personDao.getPeAndAr();
    }

    /**
     * 一个地区下的每个部门的人员统计
     */
    @Override
    public List<PersonInDeptCountDTO> getPeAndDepByAre(Map<String, Object> pADBMap) {

        return this.personDao.getPeAndDepByAre(pADBMap);
    }

    /**
     * 执法人员查询（条形图用）
     */
    @Override
    public List<LawPersonDTO> getSelectLaw() {

        return this.personDao. getSelectLaw();
    }

    /**
     * 监督人员查询(条形图用)
     */
    @Override
    public List<LawPersonDTO> getSelectSup() {

        return this.personDao.getSelectSup();
    }

    /**
     * 执法人员统计
     */
    @Override
    public List<LawPersonDTO> getAllLaw() {

        return this.personDao.getAllLaw();
    }

    /**
     * 根据区域id查询执法人员详细信息
     */
    @Override
    public PageList<DeptPersonDTO> getSelectByAreaId(DeptPersonDTO selectPersonByAreaIdDTO) {
        // TODO Auto-generated method stub
        PageList<DeptPersonDTO> page = new PageList<DeptPersonDTO>();
        int pageCount = personDao.getMessageNum(selectPersonByAreaIdDTO.getAreaName());//得到总条数
        page = initPage(page, pageCount,selectPersonByAreaIdDTO);
        List<DeptPersonDTO> message= personDao.getSelectByAreaId(selectPersonByAreaIdDTO);
        if (!message.isEmpty()) {
            page.setDatas(message);
        }
        return page;
    }

    /**
     * PS PC执法人员列表查询总数
     */
    @Override
    public PageList<DeptPersonDTO> getByAreaId(DeptPersonDTO selectPersonByAreaIdDTO) {
        // TODO Auto-generated method stub
        PageList<DeptPersonDTO> page = new PageList<DeptPersonDTO>();
        int pageCount = personDao.getMessagNum(selectPersonByAreaIdDTO.getAreaName());//得到总条数
        page = initPage(page, pageCount,selectPersonByAreaIdDTO);
        List<DeptPersonDTO> message= personDao.getByAreaId(selectPersonByAreaIdDTO);
        if (!message.isEmpty()) {
            page.setDatas(message);
        }
        return page;
    }

    private PageList<DeptPersonDTO> initPage(PageList<DeptPersonDTO> page, int pageCount,
                                                       DeptPersonDTO selectPersonByAreaIdDTO) {
        page.setTotalRecord(pageCount);
        page.setCurrentPage(selectPersonByAreaIdDTO.getCurrentPage());
        page.setPageSize(selectPersonByAreaIdDTO.getPageSize());
        selectPersonByAreaIdDTO.setStartIndexEndIndex();
        return page;
    }

    /**
     * 根据deptId查询执法人员详细信息
     */
    @Override
    public PageList<DeptPersonDTO> getSelectBydeptIdPC(DeptPersonDTO selectPersonByAreaIdDTO) {
        PageList<DeptPersonDTO> page = new PageList<DeptPersonDTO>();
        int pageCount = personDao.getMesNum(selectPersonByAreaIdDTO.getDeptId());//得到总条数
        page = initPage(page, pageCount,selectPersonByAreaIdDTO);
        List<DeptPersonDTO> message= personDao.getSelectBydeptIdPC(selectPersonByAreaIdDTO);
        if (!message.isEmpty()) {
            page.setDatas(message);
        }
        return page;
    }

    @Override
    public List<DeptPersonDTO> getSelectBydeptId(Map<String, Object> param) {
        return this.personDao.getSelectBydeptId(param);
    }

    /**
     * ps
     * 根据人员id查询执法人员详细
     * @return
     */
    @Override
    public List<DeptPersonDTO> getSelectPersonById(Map<String, Object> mapGetSelectPersonById) {
        return this.personDao.getSelectPersonById(mapGetSelectPersonById);
    }

    @Override
    public List<DeptPersonDTO> getAppPersonListByParam(Map<String, Object> param) {
        return this.personDao.getAppPersonListByParam(param);
    }


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DeptPersonEntity> page = this.page(
                new Query<DeptPersonEntity>().getPage(params),
                new QueryWrapper<DeptPersonEntity>()
        );

        return new PageUtils(page);
    }

}