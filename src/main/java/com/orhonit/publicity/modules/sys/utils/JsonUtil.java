package com.orhonit.publicity.modules.sys.utils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;

/**
 * json对象转换工具类
 */
public class JsonUtil {
	private static ObjectMapper mapper = null;

	static {
		mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}

	public static <T> T fromJson(String jsonString, Class<T> type) {
		if (StringUtils.isBlank(jsonString)) {
			return null;
		} else {
			try {
				return mapper.readValue(jsonString, type);
			} catch (IOException arg2) {
				arg2.printStackTrace();
				return null;
			}
		}
	}

	public static String toJson(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (IOException arg1) {
			arg1.printStackTrace();
			return null;
		}
	}
	
	

}