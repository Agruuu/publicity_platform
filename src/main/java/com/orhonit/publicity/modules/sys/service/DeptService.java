package com.orhonit.publicity.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.modules.sys.dto.AreaDeptDTO;
import com.orhonit.publicity.modules.sys.dto.DeptDTO;
import com.orhonit.publicity.modules.sys.entity.DeptEntity;

import java.util.List;
import java.util.Map;

/**
 * 执法主体服务类
 */
public interface DeptService extends IService<DeptEntity> {

    /**
     * 获取该主体能管理的所有主体列表
     */
    List<DeptDTO> getDeptByManageDeptIdList(Map<String, Object> paramMap);

    /**
     * 根据areaId和name查询主体列表
     */
    List<DeptDTO> getDeptList(Map<String, Object> paramMap);

    /**
     * 查询各区域下主体数量
     */
    List<AreaDeptDTO> areaDeptList();

    /**
     * 各辖区主体数量和性质统计
     */
    List<AreaDeptDTO> areaDeptProList(Map<String, Object> paramMap);

    /**
     * 根据areaId和deptId查询主体列表
     */
    List<AreaDeptDTO> getDeptAllList(Map<String, Object> paramMap);
    /**
     * 根据主体Id查询本主体及下级主体列表
     */
    List<DeptDTO> getDeptListByDeptId(Map<String, Object> paramMap);

    /**
     * 执法主体下执法人员、法律、权责、委托部门、案件信息统计
     */
    DeptDTO getCountByDeptId(Map<String, Object> paramMap);

    /**
     * 根据区域Id和区域类型查询主体
     */
    List<DeptDTO> selDeptByDeptId(Map<String, Object> paramMap);

    /**
     * 根据主体ID查询主体信息
     * */
    DeptDTO  findDeptInfoById(String deptId);

    List<DeptDTO> deptTreeByAreaId(String deptId);

    List<DeptDTO> deptTreeAll();

    List<DeptDTO> deptTreeByDeptId(String dept_id);

    PageUtils queryPage(Map<String, Object> params);
}

