package com.orhonit.publicity.modules.sys.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportIndex {
    private String name;
    private String mgl_name;
    private Integer cnumber;
}
