package com.orhonit.publicity.modules.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 执法主体
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DeptDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private String id;
	/**
	 * 主体编号
	 */
	private String code;
	/**
	 * 上级主体
	 */
	private String parentId;
	/**
	 * 地区ID
	 */
	private String areaId;
	/**
	 * 地区名称
	 */
	private String areaName;
	/**
	 * 主体名称全拼音
	 */
	private String nameSpell;
	/**
	 * 主体名称
	 */
	private String name;
	/**
	 * 主体名称简称
	 */
	private String shortName;
	/**
	 * 主体地址
	 */
	private String address;
	/**
	 * 法定代表人
	 */
	private String legalPerson;
	/**
	 * 机构职能
	 */
	private String agencyFunctions;
	/**
	 * 监督投诉电话
	 */
	private String monitoringComplaints;
	/**
	 * 通信地址
	 */
	private String mailingAddress;
	/**
	 * 主体级别
	 */
	private Integer level;
	/**
	 * 主体 性质：1授权执法单位 2委托执法单位 3行政执法机关
	 */
	private Integer deptProperty;
	/**
	 * 执法性质。1执法，2监督
	 */
	private String lawType;
	/**
	 * 是否公示  0： 否  1：是
	 */
	private String isPs;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 电话
	 */
	private String tel;
	/**
	 * 执法区域ID（预留）说明：如果执法区域可以细化到社区或小区，可以单独维护一张执法区域表，结合gis展示执法区域
	 */
	private String lawArea;
	/**
	 * 是否启用
	 */
	private String ifEffect;
	/**
	 * 删除标记
	 */
	private String delFlag;
	/**
	 * 主体名称（蒙文）
	 */
	private String mglName;
	/**
	 * 主体名称简称（蒙文）
	 */
	private String mglShortName;
	/**
	 * 地址(蒙文)
	 */
	private String mglAddress;
	/**
	 * 法定代表人(蒙文)
	 */
	private String mglLegalPerson;
	/**
	 * 创建者名称(蒙文)
	 */
	private String mglCreateName;
	/**
	 * 更新人姓名(蒙文)
	 */
	private String mglUpdateName;
	/**
	 * 
	 */
	private String mglLawarea;
	/**
	 * 创建者用户名
	 */
	private String createBy;
	/**
	 * 创建者名称
	 */
	private String createName;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 更新人登录名
	 */
	private String updateBy;
	/**
	 * 更新人姓名
	 */
	private String updateName;
	/**
	 * 更新时间
	 */
	private Date updateDate;

	//执法人员总数
	private String personCount;
	//法律总数
//	private String lawCount;
	//委托机构总数
	private String deptAgentCount;
	//权责总数
//	private String potenceCount;
	//案件总数
	private String caseCount;

}
