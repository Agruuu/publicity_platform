package com.orhonit.publicity.modules.sys.dao;

import com.orhonit.publicity.modules.sys.model.Person;
import com.orhonit.publicity.modules.sys.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserDao {

	@Options(useGeneratedKeys = true, keyProperty = "id")
	@Insert("insert into sys_user(username, password, salt, nickname, headImgUrl, phone, telephone, email, birthday, sex, status, createTime, updateTime, dept_id, person_id, area_id) values(#{username}, #{password}, #{salt}, #{nickname}, #{headImgUrl}, #{phone}, #{telephone}, #{email}, #{birthday}, #{sex}, #{status}, now(), now(), #{dept_id}, #{person_id}, #{area_id})")
	int save(User user);

	@Select("select t.*,k.name deptName from sys_user t  LEFT JOIN dept k on t.dept_id=k.id  where t.id = #{id}")
	User getById(Long id);

	@Select("select t.*,k.name deptName from sys_user t  LEFT JOIN dept k on t.dept_id=k.id  where t.username = #{username}")
	User getUser(@Param("username") String username);

	@Update("update sys_user t set t.status = #{status} where t.id = #{id}")
	int updateStatus(@Param("id") Long id, @Param("status") Integer status);

	@Update("update sys_user t set t.password = #{password} where t.id = #{id}")
	int changePassword(@Param("id") Long id, @Param("password") String password);

	Integer count(@Param("params") Map<String, Object> params);

	List<User> list(@Param("params") Map<String, Object> params, @Param("start") Integer start,
                    @Param("length") Integer length);

	@Delete("delete from sys_role_user where userId = #{userId}")
	int deleteUserRole(Long userId);

	int saveUserRoles(@Param("userId") Long userId, @Param("roleIds") List<Long> roleIds);

	int update(User user);

	@Select("select * from sys_user t where t.username = #{username} and t.password = #{password}")
	User getfindByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
	
	@Delete("delete from sys_user where id = #{id}")
	int delete(Long id);
	
	@Select("select * from sys_user t where t.person_id = #{personId}")
	User getUserByPersonId(@Param("personId") String personId);
	
	@Select("select picture from dept_person t where t.id = #{personId}")
	String getImgByPersonId(@Param("personId") String personId);
	
	List<Object> selRoleByPersonNum(String username);
	
	@Select("select * from dept_person t where t.id = #{personId}")
	Person getPersonById(@Param("personId") String personId);
}
