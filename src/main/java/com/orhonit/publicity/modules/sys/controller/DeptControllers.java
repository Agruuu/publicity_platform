package com.orhonit.publicity.modules.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.orhonit.publicity.common.constants.ResultCode;
import com.orhonit.publicity.common.model.Result;
import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.R;
import com.orhonit.publicity.common.utils.ResultUtil;
import com.orhonit.publicity.modules.sys.dto.AreaDeptDTO;
import com.orhonit.publicity.modules.sys.dto.DeptDTO;
import com.orhonit.publicity.modules.sys.entity.DeptEntity;
import com.orhonit.publicity.modules.sys.service.DeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 执法主体
 */
@RestController
@RequestMapping("sys/dept")
public class DeptControllers {
    @Autowired
    private DeptService deptService;

    /**
     * 根据区域ID和主体名称查询主体列表
     */
    @GetMapping("/deptList")
    public Result<List<DeptDTO>> getDeptList(@RequestParam(value="areaId",required = false) String areaId,
                                             @RequestParam(value="name", required = false) String name,
                                             @RequestParam(value="deptProperty", required = false) String deptProperty ) {

        Map<String, Object> params = new HashMap<>();
        params.put("areaId", areaId);
        params.put("name", name);
        params.put("deptProperty", deptProperty);
        List<DeptDTO> deptDTOList = this.deptService.getDeptList(params);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, deptDTOList);
    }

    /**
     * 获取该主体能管理的所有主体列表
     */
    @GetMapping("/deptManageList")
    public Result<List<DeptDTO>> getDeptManageList(@RequestBody() Map<String, Object> params) {

        List<DeptDTO> deptDTOList = this.deptService.getDeptByManageDeptIdList(params);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, deptDTOList);
    }

    /**
     * 各辖区主体数量统计
     */
    @GetMapping("/areadeptList")
    public Result<List<AreaDeptDTO>> getDeptList() {

        List<AreaDeptDTO> areaDeptDTOList = this.deptService.areaDeptList();
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, areaDeptDTOList);
    }

    /**
     * 各辖区主体数量和性质统计
     */
    @GetMapping("/areadeptProList")
    public Result<List<AreaDeptDTO>> areaDeptProList(@RequestParam(value="areaId",required = false) String areaId) {

        Map<String, Object> params = new HashMap<>();
        params.put("areaId", areaId);

        List<AreaDeptDTO> areaDeptDTOList = this.deptService.areaDeptProList(params);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, areaDeptDTOList);
    }

    /**
     * 根据区域ID和主体名称查询主体列表
     */
    @GetMapping("/deptAllList")
    public Result<List<AreaDeptDTO>> getDeptAllList(@RequestParam(value="areaId",required = false) String areaId,
                                                      @RequestParam(value="deptId", required = false) String deptId ) {

        Map<String, Object> params = new HashMap<>();
        params.put("areaId", areaId);
        params.put("deptId", deptId);

        List<AreaDeptDTO> areaDeptDTOList = this.deptService.getDeptAllList(params);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, areaDeptDTOList);
    }

    /**
     * 根据主体Id查询本主体及下级主体列表
     */
    @GetMapping("/getDeptListByDeptId")
    public Result<List<DeptDTO>> getDeptListByDeptId(@RequestParam(value="deptId", required = true) String deptId ) {

        Map<String, Object> params = new HashMap<>();
        params.put("deptId", deptId);

        List<DeptDTO> deptDTOList = this.deptService.getDeptListByDeptId(params);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, deptDTOList);
    }

    /**
     * 执法主体下执法人员、法律、权责、委托部门、案件信息统计
     */
    @GetMapping("/getCountByDeptId")
    public Result<DeptDTO> getCountByDeptId(@RequestParam(value="deptId", required = false) String deptId ) {

        Map<String, Object> params = new HashMap<>();
        params.put("deptId", deptId);

        DeptDTO deptDTOList = this.deptService.getCountByDeptId(params);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, deptDTOList);
    }

    /**
     * 根据区域Id和区域类型查询主体
     */
    @GetMapping("/selDeptByDeptId")
    public Result<List<DeptDTO>> selDeptByDeptId(@RequestParam(value="areaId", required = true) String areaId,
                                                       @RequestParam(value="deptProperty", required = false) String deptProperty ) {

        Map<String, Object> params = new HashMap<>();
        params.put("areaId", areaId);
        params.put("deptProperty", deptProperty);
        List<DeptDTO> deptDTOList = this.deptService.selDeptByDeptId(params);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, deptDTOList);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:dept:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = deptService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:dept:info")
    public R info(@PathVariable("id") String id){
		DeptEntity dept = deptService.getById(id);

        return R.ok().put("dept", dept);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:dept:save")
    public R save(@RequestBody DeptEntity dept){
		deptService.save(dept);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:dept:update")
    public R update(@RequestBody DeptEntity dept){
		deptService.updateById(dept);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:dept:delete")
    public R delete(@RequestBody String[] ids){
		deptService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
