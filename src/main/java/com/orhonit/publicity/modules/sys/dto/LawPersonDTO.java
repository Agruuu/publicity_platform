package com.orhonit.publicity.modules.sys.dto;

import lombok.Data;

/**
 * 执法人员查询（条形图用）
 */
@Data
public class LawPersonDTO {
	private String areaId;
	private String areaName;
	private String personCount;
	private String lawType;
}
