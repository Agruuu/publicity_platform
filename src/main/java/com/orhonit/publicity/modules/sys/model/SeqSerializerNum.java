package com.orhonit.publicity.modules.sys.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="ole_ef_seq")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SeqSerializerNum {

	@Id
	private String seqName;
	
	private String seqValue;
	
	private Date seqDate;
}
