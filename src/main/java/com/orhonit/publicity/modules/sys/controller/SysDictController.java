package com.orhonit.publicity.modules.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.orhonit.publicity.common.annotation.LogAnnotation;
import com.orhonit.publicity.common.constants.ResultCode;
import com.orhonit.publicity.common.datatables.TableRequest;
import com.orhonit.publicity.common.datatables.TableRequestHandler;
import com.orhonit.publicity.common.datatables.TableResponse;
import com.orhonit.publicity.common.model.Result;
import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.R;
import com.orhonit.publicity.common.utils.ResultUtil;
import com.orhonit.publicity.modules.sys.config.DictCacheManager;
import com.orhonit.publicity.modules.sys.dto.SysDictDTO;
import com.orhonit.publicity.modules.sys.entity.SysDictEntity;
import com.orhonit.publicity.modules.sys.service.SysDictService;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 系统字典
 */
@RestController
@RequestMapping("sys/sysDict")
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;

//    @Autowired
//    private DictCacheManager dictCacheManager;


    @LogAnnotation
    @PostMapping
    @ApiOperation(value = "保存字典")
    public void saveSysDict(@RequestBody SysDictEntity sysDict) {
        sysDictService.saveSysDict(sysDict);

        List<SysDictEntity> sysDictEntities = null;//this.dictCacheManager.getDictTypeByValue(sysDict.getTypeValue());

        if ( sysDictEntities != null ) {

//            this.dictCacheManager.deleteDictByTypeValue(sysDict.getTypeValue());
        }
    }

    @LogAnnotation
    @PutMapping
    @ApiOperation(value = "修改字典")
    public void updateSysDict(@RequestBody SysDictEntity sysDict) {
        sysDictService.updateSysDict(sysDict);
        List<SysDictEntity> sysDictEntities = null;//this.dictCacheManager.getDictTypeByValue(sysDict.getTypeValue());

        if ( sysDictEntities != null ) {

//            this.dictCacheManager.deleteDictByTypeValue(sysDict.getTypeValue());
//            this.dictCacheManager.deleteDictByTypeValue(sysDict.getTypeValue());
        }
    }

    @GetMapping(value="/list")
    @ApiOperation(value = "字典列表")
    public TableResponse<SysDictEntity> listDicts(TableRequest request) {
        return TableRequestHandler.<SysDictEntity> builder().countHandler(new TableRequestHandler.CountHandler() {
            @Override
            public int count(TableRequest request) {

				/*Integer count = dictCacheManager.getSysDictCount("count");

				if ( count == null ) {
					count = sysDictService.getSysDictCount(request.getParams());

					dictCacheManager.saveDictCount(count, "count");
				}*/



                return sysDictService.getSysDictCount(request.getParams());
            }
        }).listHandler(new TableRequestHandler.ListHandler<SysDictEntity>() {
            @Override
            public List<SysDictEntity> list(TableRequest request) {

                return sysDictService.getSysDictList(request.getParams(), request.getStart(), request.getLength());

				/*List<SysDictEntity> list = dictCacheManager.getSysDictList("list");

				if ( list == null ) {
					list = sysDictService.getSysDictList(request.getParams(), request.getStart(), request.getLength());

					List<SysDictEntity> cacheList = sysDictService.getSysDictList(request.getParams(), null, null);

					dictCacheManager.saveDictList(cacheList, "list");
				} else if (request.getStart() != null && request.getLength() != null) {

					Integer paramStart = request.getStart();

					Integer paramLength = request.getLength();

					if ( paramStart.intValue() > list.size() ) {
						return new ArrayList<>();
					}

					if ( paramLength.intValue() > list.size() ) {
						paramLength = list.size();
					}

					if ( paramStart.intValue() + paramLength.intValue() > list.size() ) {
						paramLength = list.size() - paramStart.intValue();
					}

					return list.subList(paramStart.intValue(), paramStart.intValue() + paramLength.intValue() );

				}

				return list;*/
            }
        }).build().handle(request);
    }

    @LogAnnotation
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除字典")
    public void delete(@PathVariable String id) {

        SysDictEntity entity = this.sysDictService.getById(id);

        sysDictService.delSysDict(id);

//        List<SysDictEntity> sysDictEntities = this.dictCacheManager.getDictTypeByValue(entity.getTypeValue());

//        if ( sysDictEntities != null ) {
//
//            this.dictCacheManager.deleteDictByTypeValue(entity.getTypeValue());
//        }

    }

    @ApiOperation(value = "根据id获取字典详细信息")
    @GetMapping("/{id}")
    @RequiresPermissions("sys:user:query")
    public SysDictDTO dict(@PathVariable String id) {
        SysDictDTO dto = new SysDictDTO();
        BeanUtils.copyProperties(sysDictService.getById(id), dto);
        return dto;
    }

    @GetMapping(value="/list/{typeValue}")
    public Result<Object> getDictByTypeValue(@PathVariable String typeValue) {

        //TODO
        List<SysDictEntity> sysDictEntities = null;//this.dictCacheManager.getDictTypeByValue(typeValue);

        if ( sysDictEntities == null ) {
            sysDictEntities = this.sysDictService.getDictByTypeValue(typeValue);

//            this.dictCacheManager.saveDictTypeValueData(typeValue, sysDictEntities);
        }


        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, sysDictEntities);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:sysDict:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysDictService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:sysDict:info")
    public R info(@PathVariable("id") String id){
		SysDictEntity sysDict = sysDictService.getById(id);

        return R.ok().put("sysDict", sysDict);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:sysDict:save")
    public R save(@RequestBody SysDictEntity sysDict){
		sysDictService.save(sysDict);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:sysDict:update")
    public R update(@RequestBody SysDictEntity sysDict){
		sysDictService.updateById(sysDict);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:sysDict:delete")
    public R delete(@RequestBody String[] ids){
		sysDictService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
