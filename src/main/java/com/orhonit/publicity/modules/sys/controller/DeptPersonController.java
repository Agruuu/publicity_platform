package com.orhonit.publicity.modules.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.orhonit.publicity.common.constants.ResultCode;
import com.orhonit.publicity.common.model.Result;
import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.R;
import com.orhonit.publicity.common.utils.ResultUtil;
import com.orhonit.publicity.modules.sys.dto.DeptPersonDTO;
import com.orhonit.publicity.modules.sys.dto.LawPersonDTO;
import com.orhonit.publicity.modules.sys.dto.PersonInAreaCountDTO;
import com.orhonit.publicity.modules.sys.dto.PersonInDeptCountDTO;
import com.orhonit.publicity.modules.sys.entity.DeptPersonEntity;
import com.orhonit.publicity.modules.sys.service.DeptPersonService;
import com.orhonit.publicity.modules.sys.utils.PageList;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 主体人员表
 * 执法人员列表控制器(Lists)
 */
@RestController
@RequestMapping("sys/deptPerson")
public class DeptPersonController {

    @Autowired
    private DeptPersonService deptPersonService;

    /**
     * 简要说明以及逻辑
     * @param param 入参
     * @return 返回值
     */
    @GetMapping("/find")
    public Result<List<DeptPersonDTO>> getPersonListByParam(@PathVariable Map<String,Object> param){

        List<DeptPersonDTO> deptList = deptPersonService.getPersonListByParam(param);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, deptList);
    }


    /**
     * 执法人员查询
     * @param certNum 执法证账件号 入参
     * @param name 执法人员姓名 入参
     * @param deptId 部门ID 入参
     * @param lawType 执法类型 入参
     *
     */
    @GetMapping("/getPerDTO")
    public Result<List<DeptPersonDTO>> getPerDTO(
            @RequestParam(value="certNum",defaultValue="",required=false) String certNum,
            @RequestParam(value="name",defaultValue="",required=false) String name,
            @RequestParam(value="deptId",defaultValue="",required=false) String deptId,
            @RequestParam(value="lawType",defaultValue="",required=false) String lawType){

        Map<String, Object> ParamMap = new HashMap<String, Object>();
        ParamMap.put("certNum", certNum);
        ParamMap.put("name", name);
        ParamMap.put("deptId", deptId);
        ParamMap.put("lawType", lawType);

        List<DeptPersonDTO> perMap=this.deptPersonService.getPerDTO(ParamMap);

        return (Result) ResultUtil.toResponseWithData(ResultCode.SUCCESS, perMap);

    }

    /**
     * 执法人员详细
     * @param personId 执法人员ID
     */
    @GetMapping("/getByPersonId")
    public Result<List<DeptPersonDTO>> getSelectByPersonId(
            @RequestParam(value="personId",defaultValue="",required=false) String personId
    ){

        Map<String, Object> personIdMp = new HashMap<>();
        personIdMp.put("personId", personId);

        List<DeptPersonDTO> pmPersonId = this.deptPersonService.getSelectByPersonId(personIdMp);

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, pmPersonId);

    }

    @GetMapping("/getPeAndArSelect")
    public Result<List<PersonInAreaCountDTO>> getPeAndArSelect(){
        List<PersonInAreaCountDTO> PeAndSelectlist =this.deptPersonService.getPeAndArSelect();

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, PeAndSelectlist);

    }

    /**
     * PC各个区域执法人员数量统计
     */
    @GetMapping("/getPeAndAr")
    public Result<List<PersonInAreaCountDTO>> getPeAndAr(){
        List<PersonInAreaCountDTO> PeAndSelectlist =this.deptPersonService.getPeAndAr();

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, PeAndSelectlist);

    }

    /**
     * 各个区域执法主体性质分类统计(条形图用)
     */
    @GetMapping("/getSelectSup")
    public Result<List<LawPersonDTO>> getSelectSup(){

        List<LawPersonDTO> selectLaw = this.deptPersonService.getSelectLaw();

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, selectLaw);

    }

    /**
     * 各个区域执法主体性质分类统计(条形图用)
     */
    @GetMapping("/getSelectCount")
    public Result<List<LawPersonDTO>> getSelectCount(){

        List<LawPersonDTO> selectLaw = this.deptPersonService.getSelectSup();

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, selectLaw);

    }

    /**
     * 各个区域执法人员统计
     */
    @GetMapping("/getAllLaw")
    public Result<List<LawPersonDTO>> getAllLaw(){

        List<LawPersonDTO> selectLaw = this.deptPersonService.getAllLaw();

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, selectLaw);

    }

    /**
     * 一个地区下的每个部门的人员统计
     * @param areaId 区域ID
     */
    @GetMapping("/getPeAndDepByAre")
    public Result<List<PersonInDeptCountDTO>> getPeAndDepByAre(
            @RequestParam(value="",defaultValue="",required=false) String areaId){

        Map<String, Object> PADBMap = new HashMap<>();
        PADBMap.put("areaId", areaId);

        List<PersonInDeptCountDTO> MapPADBList=this.deptPersonService.getPeAndDepByAre(PADBMap);

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, MapPADBList);

    }

    /**
     * 根据区域name查询执法人员详细信息
     */
    @GetMapping("/getSelectByAreaId")
    public Result<PageList<DeptPersonDTO>> getSelectByAreaId(
            @RequestParam(value="areaName",defaultValue="",required=false) String areaName,
            @RequestParam(value="currentPage",defaultValue="",required=false) int currentPage,
            @RequestParam(value="pageSize",defaultValue="",required=false) int pageSize){
        DeptPersonDTO selectPersonByAreaIdDTO =new DeptPersonDTO();
        if(currentPage != 0 & pageSize != 0){
            selectPersonByAreaIdDTO.setAreaName(areaName);
            selectPersonByAreaIdDTO.setCurrentPage(currentPage);
            selectPersonByAreaIdDTO.setPageSize(pageSize);
        }else{
            selectPersonByAreaIdDTO.setAreaName(areaName);
            selectPersonByAreaIdDTO.setCurrentPage(1);
            selectPersonByAreaIdDTO.setPageSize(20);
        }

        PageList<DeptPersonDTO> SelectByAreaIdMap = this.deptPersonService.getSelectByAreaId(selectPersonByAreaIdDTO);

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, SelectByAreaIdMap);

    }

    /**
     * 根据区域name查询执法人员详细信息
     */
    @GetMapping("/getByAreaId")
    public Result<PageList<DeptPersonDTO>> getByAreaId(
            @RequestParam(value="areaName",defaultValue="",required=false) String areaName,
            @RequestParam(value="currentPage",defaultValue="",required=false) int currentPage,
            @RequestParam(value="pageSize",defaultValue="",required=false) int pageSize){
        DeptPersonDTO selectPersonByAreaIdDTO =new DeptPersonDTO();
        if(currentPage != 0 & pageSize != 0){
            selectPersonByAreaIdDTO.setAreaName(areaName);
            selectPersonByAreaIdDTO.setCurrentPage(currentPage);
            selectPersonByAreaIdDTO.setPageSize(pageSize);
        }else{
            selectPersonByAreaIdDTO.setAreaName(areaName);
            selectPersonByAreaIdDTO.setCurrentPage(1);
            selectPersonByAreaIdDTO.setPageSize(20);
        }

        PageList<DeptPersonDTO> SelectByAreaIdMap = this.deptPersonService.getByAreaId(selectPersonByAreaIdDTO);

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, SelectByAreaIdMap);

    }

    /**
     * 根据deptId查询执法人员详细信息
     */
    @GetMapping("/getSelectBydeptIdPC")
    public Result<PageList<DeptPersonDTO>> getSelectBydeptIdPC(
            @RequestParam(value="deptId",defaultValue="",required=false) String deptId,
            @RequestParam(value="currentPage",defaultValue="",required=false) int currentPage,
            @RequestParam(value="pageSize",defaultValue="",required=false) int pageSize){


        DeptPersonDTO selectPersonByAreaIdDTO =new DeptPersonDTO();
        if(currentPage != 0 & pageSize != 0){
            selectPersonByAreaIdDTO.setDeptId(deptId);
            selectPersonByAreaIdDTO.setCurrentPage(currentPage);
            selectPersonByAreaIdDTO.setPageSize(pageSize);
        }else{
            selectPersonByAreaIdDTO.setDeptId(deptId);
            selectPersonByAreaIdDTO.setCurrentPage(1);
            selectPersonByAreaIdDTO.setPageSize(20);
        }

        PageList<DeptPersonDTO> SelectByAreaIdMap = this.deptPersonService.getSelectBydeptIdPC(selectPersonByAreaIdDTO);

        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, SelectByAreaIdMap);

    }

    /**
     * 根据deptId查询执法人员详细信息 手机端
     */
    @GetMapping("/getSelectBydeptId")
    public Result<List<DeptPersonDTO>> getSelectBydeptId(
            @RequestParam(value="deptId",defaultValue="",required=false) String deptId){
        Map<String, Object> map = new HashMap<>();
        map.put("deptId", deptId);
        List<DeptPersonDTO> SelectByAreaIdMap = this.deptPersonService.getSelectBydeptId(map);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, SelectByAreaIdMap);

    }

    /**
     * 根据人员id查询执法人员详细
     * @param personId
     */
    @GetMapping("/getSelectPersonById")
    public Result<List<DeptPersonDTO>> getSelectPersonById(
            @RequestParam(value="personId",defaultValue="",required=false) String personId
    ){
        Map<String, Object> mapGetSelectPersonById = new HashMap<>();
        mapGetSelectPersonById.put("personId", personId);
        List<DeptPersonDTO> selectPersonByIdMap = this.deptPersonService.getSelectPersonById(mapGetSelectPersonById);
        return ResultUtil.toResponseWithData(ResultCode.SUCCESS, selectPersonByIdMap);

    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:deptPerson:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = deptPersonService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:deptPerson:info")
    public R info(@PathVariable("id") String id){
		DeptPersonEntity deptPerson = deptPersonService.getById(id);

        return R.ok().put("deptPerson", deptPerson);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:deptPerson:save")
    public R save(@RequestBody DeptPersonEntity deptPerson){
		deptPersonService.save(deptPerson);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:deptPerson:update")
    public R update(@RequestBody DeptPersonEntity deptPerson){
		deptPersonService.updateById(deptPerson);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:deptPerson:delete")
    public R delete(@RequestBody String[] ids){
		deptPersonService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
