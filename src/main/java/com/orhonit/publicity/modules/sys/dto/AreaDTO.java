package com.orhonit.publicity.modules.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 行政区划(区域)表
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AreaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	private Integer id;
	/**
	 * 编码
	 */
	private String code;
	/**
	 * 
	 */
	private String level;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 父级地区
	 */
	private String parentId;
	/**
	 * 英文名
	 */
	private String nameEn;
	/**
	 * 排序
	 */
	private String sort;
	/**
	 * 区域面积
	 */
	private Double area;
	/**
	 * 是否启用
	 */
	private String isEffect;
	/**
	 * 删除标志
	 */
	private String delFlag;
	/**
	 * 创建人登录名称
	 */
	private String createBy;
	/**
	 * 创建日期
	 */
	private Date createDate;
	/**
	 * 更新人登录名称
	 */
	private String updateBy;
	/**
	 * 更新日期
	 */
	private Date updateDate;
	/**
	 * 创建人名称
	 */
	private String createName;
	/**
	 * 更新人名称
	 */
	private String updateName;
	/**
	 * 区划名称_蒙文
	 */
	private String mglName;
	/**
	 * 创建人名称(蒙文)
	 */
	private String mglCreateName;
	/**
	 * 更新人名称(蒙文)
	 */
	private String mglUpdateName;

}
