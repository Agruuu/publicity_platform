package com.orhonit.publicity.modules.sys.dto;

import lombok.Data;

/**
 * 一个地区下的每个部门的人员统计
 */
@Data
public class PersonInDeptCountDTO {
	
	private String depId;
	private String name;
	private String personCount;
	private String lawType;

}
