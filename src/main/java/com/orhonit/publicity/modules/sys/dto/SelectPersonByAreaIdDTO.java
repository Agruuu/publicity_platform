package com.orhonit.publicity.modules.sys.dto;

import com.orhonit.publicity.common.model.PageEntity;
import lombok.Data;

/**
 * 执法人员详细信息
 */
@Data
public class SelectPersonByAreaIdDTO extends PageEntity {
	private String id;
	private String name;
	private String sex;
	private String duty;
	private String deptId;
	private String deptName;
	private String lawType;
	private String certNum;
	private String lawArea;
	private String areaName;
	private String picture;
	private String nameMgl;
	private String deptNameMgl;
}
