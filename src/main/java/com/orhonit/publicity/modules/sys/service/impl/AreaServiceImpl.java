package com.orhonit.publicity.modules.sys.service.impl;

import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.Query;
import com.orhonit.publicity.modules.sys.dao.AreaDao;
import com.orhonit.publicity.modules.sys.entity.AreaEntity;
import com.orhonit.publicity.modules.sys.service.AreaService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("areaService")
public class AreaServiceImpl extends ServiceImpl<AreaDao, AreaEntity> implements AreaService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AreaEntity> page = this.page(
                new Query<AreaEntity>().getPage(params),
                new QueryWrapper<AreaEntity>()
        );

        return new PageUtils(page);
    }

}