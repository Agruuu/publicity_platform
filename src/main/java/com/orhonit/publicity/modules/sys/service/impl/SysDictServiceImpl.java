package com.orhonit.publicity.modules.sys.service.impl;

import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.common.utils.Query;
import com.orhonit.publicity.modules.sys.dao.SysDictDao;
import com.orhonit.publicity.modules.sys.entity.SysDictEntity;
import com.orhonit.publicity.modules.sys.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("sysDictService")
public class SysDictServiceImpl extends ServiceImpl<SysDictDao, SysDictEntity> implements SysDictService {

    @Autowired
    private SysDictDao dictDao;

    @Override
    public void saveSysDict(SysDictEntity sysDictEntity) {
        sysDictEntity.setId(UUID.randomUUID().toString());
        sysDictEntity.setCreatedDate(new Date());
//        sysDictRepository.save(sysDictEntity);
        this.dictDao.insert(sysDictEntity);
    }

    @Override
    public void updateSysDict(SysDictEntity sysDict) {
        sysDict.setLastUpdate(new Date());
//        this.sysDictRepository.save(sysDict);
        this.dictDao.updateById(sysDict);
    }

    @Override
    public int getSysDictCount(Map<String, Object> params) {
        return dictDao.count(params);
    }

    @Override
    public List<SysDictEntity> getSysDictList(Map<String, Object> params, Integer start, Integer length) {
        List<SysDictEntity> list =this.dictDao.list(params, start,length);
        return list;
    }

    @Override
    public SysDictEntity getById(String id) {
//        return  sysDictRepository.getOne(id);
        return dictDao.selectById(id);

    }

    @Override
    public List<SysDictEntity> getDictByTypeValue(String typeValue) {
//        return this.sysDictRepository.findByTypeValueOrderBySortAsc(typeValue);
        Map<String, Object> params = new HashMap<> ();
        params.put("typeValue", typeValue);
        return this.dictDao.findByTypeValueOrderBySortAsc(params);
    }

    @Override
    public String getDescByValue(String typeValue, String enumValue) {
        String desc=dictDao.getDescByValue(typeValue,enumValue);
        return desc;
    }

    @Override
    public void delSysDict(String id) {
        dictDao.deleteById(id);
//        sysDictRepository.deleteById(id);
    }

    @Override
    public Map<String, String> getDictMapByTypeValue(String typeValue) {
        Map<String, Object> params = new HashMap<> ();
        params.put("typeValue", typeValue);
//        List<SysDictEntity> dicts = this.sysDictRepository.findByTypeValueOrderBySortAsc(typeValue);
        List<SysDictEntity> dicts = this.dictDao.findByTypeValueOrderBySortAsc(params);
        Map<String, String> dictMap = new HashMap<String, String>();
        for (SysDictEntity sysDictEntity : dicts) {
            dictMap.put(sysDictEntity.getEnumValue(), sysDictEntity.getEnumDesc());
        }
        return dictMap;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SysDictEntity> page = this.page(
                new Query<SysDictEntity>().getPage(params),
                new QueryWrapper<SysDictEntity>()
        );

        return new PageUtils(page);
    }

}