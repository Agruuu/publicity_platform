package com.orhonit.publicity.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 系统字典
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@TableName("sys_dict")
public class SysDictEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	private String id;
	/**
	 * 大类值
	 */
	private String typeValue;
	/**
	 * 大类描述
	 */
	private String typeDesc;
	/**
	 * 键值
	 */
	private String enumValue;
	/**
	 * 描述
	 */
	private String enumDesc;
	/**
	 * 创建时间
	 */
	private Date createdDate;
	/**
	 * 更新时间
	 */
	private Date lastUpdate;
	/**
	 * 语言
	 */
	private String lang;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 蒙文描述
	 */
	private String mglEnumDesc;
	/**
	 * 蒙文大类描述
	 */
	private String mglTypeDesc;

}
