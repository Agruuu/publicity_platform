package com.orhonit.publicity.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.orhonit.publicity.common.utils.PageUtils;
import com.orhonit.publicity.modules.sys.dto.DeptPersonDTO;
import com.orhonit.publicity.modules.sys.dto.LawPersonDTO;
import com.orhonit.publicity.modules.sys.dto.PersonInAreaCountDTO;
import com.orhonit.publicity.modules.sys.dto.PersonInDeptCountDTO;
import com.orhonit.publicity.modules.sys.entity.DeptPersonEntity;
import com.orhonit.publicity.modules.sys.model.User;
import com.orhonit.publicity.modules.sys.utils.PageList;

import java.util.List;
import java.util.Map;

/**
 * 主体人员表
 */
public interface DeptPersonService extends IService<DeptPersonEntity> {

    /**
     * 根据部门ID获取部门下的人员信息
     */
    List<DeptPersonDTO> getPersonListByParam(Map<String, Object> paramMap);

    /**
     * 执法人员查询
     */
    List<DeptPersonDTO> getPerDTO(Map<String, Object> ping);

    /**
     * 执法人员详细
     */
    List<DeptPersonDTO> getSelectByPersonId(Map<String, Object> personIdMp);
//	String getSelectByPersonId(String personId);

    /**
     * 各个区域执法人员执法类型数量统计
     */
    List<PersonInAreaCountDTO> getPeAndArSelect();

    /**
     * PC各个区域执法人员数量统计
     */
    List<PersonInAreaCountDTO> getPeAndAr();

    /**
     * 一个地区下的每个部门的人员统计
     */
    List<PersonInDeptCountDTO> getPeAndDepByAre(Map<String, Object> pADBMap);

    /**
     * 执法人员查询（条形图用）
     */
    List<LawPersonDTO> getSelectLaw();

    /**
     * 监督人员查询(条形图用)
     */
    List<LawPersonDTO> getSelectSup();

    /**
     * 执法人员统计
     */
    List<LawPersonDTO> getAllLaw();

    /**
     * 根据区域id查询执法人员详细信息
     */
    PageList<DeptPersonDTO> getSelectByAreaId(DeptPersonDTO selectPersonByAreaIdDTO);

    /**
     * PS PC执法人员列表查询总数
     */
    PageList<DeptPersonDTO> getByAreaId(DeptPersonDTO selectPersonByAreaIdDTO);

    /**
     * 根据deptId查询执法人员详细信息
     */
    PageList<DeptPersonDTO> getSelectBydeptIdPC(DeptPersonDTO selectPersonByAreaIdDTO);

    /**
     * 根据deptId查询执法人员详细信息
     */
    List<DeptPersonDTO> getSelectBydeptId(Map<String, Object> param);

    /**
     * ps
     * 根据人员id查询执法人员详细
     */
    List<DeptPersonDTO> getSelectPersonById(Map<String, Object> mapGetSelectPersonById);

    PageUtils queryPage(Map<String, Object> params);

    /**
     * app接口,获取协办人员
     */
    List<DeptPersonDTO> getAppPersonListByParam(Map<String, Object> param);


//    /**
//     * 根据区域id查询执法人员详细信息
//     * @param areaId 区域ID
//     * @return
//     */
//    PageList<SelectPersonByAreaIdDTO> getSelectByAreaId(SelectPersonByAreaIdDTO selectPersonByAreaIdDTO);
//    /**
//     * PS PC执法人员列表查询总数
//     * @param areaId 区域ID
//     * @return
//     */
//    PageList<SelectPersonByAreaIdDTO> getByAreaId(SelectPersonByAreaIdDTO selectPersonByAreaIdDTO);
//    /**
//     * 根据deptId查询执法人员详细信息
//     * @param areaId 区域ID
//     * @return
//     */
//    PageList<SelectPersonByAreaIdDTO> getSelectBydeptIdPC(SelectPersonByAreaIdDTO selectPersonByAreaIdDTO);
//    /**
//     * 根据deptId查询执法人员详细信息
//     * @param areaId 区域ID
//     * @return
//     */
//    List<SelectPersonByAreaIdDTO> getSelectBydeptId(Map<String, Object> param);
//    /**
//     * ps
//     * 根据人员id查询执法人员详细
//     * @param personId
//     * @return
//     */
//    List<SelectPersonById> getSelectPersonById(Map<String, Object> mapGetSelectPersonById);
//    /**
//     * 发送短信验证码
//     */
//    Map<Object, Object> getSmsCode(DeptPersonDTO personDTO, User user);
}

