package com.orhonit.publicity.modules.sys.dto;

import lombok.Data;

/**
 * 各个区域执法人员执法类型数量统计
 */
@Data
public class PersonInAreaCountDTO {

	private String deptId;
	private String areaName;
	private String personCount;
	private String lawType;
	private String deptName;
	private String deptNameMgl;
	private String deptShortNameMgl;

}
