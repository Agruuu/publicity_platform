package com.orhonit.publicity.common.model;

import lombok.Data;

@Data
public class PageEntity {

    /**
     * 当前页
     */
    private int currentPage;

    /**
     * 一页多少条记录
     */
    private int pageSize;

    /**
     * 从哪一行开始
     */
    private int startIndex;

    /**
     * 从哪一行结束
     */
    private int endIndex;

    /**
     * 根据当前所在页数和每页显示记录数计算出startIndex和endIndex
     */
    public void setStartIndexEndIndex(){
        this.startIndex=(this.getCurrentPage()-1)*this.getPageSize();
        this.endIndex= (this.getCurrentPage()-1)*this.getPageSize()+this.getPageSize();
    }
}
