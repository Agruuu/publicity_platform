package com.orhonit.publicity.common.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Result<T>
{
    private Integer code;

    private String msg;

    private T data;

    @Override
	public String toString() {
		return "Result [data=" + data + "]";
	}
}
