package com.orhonit.publicity.common.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 分页对象
 */

@Data
@Builder
public class Page<T> implements Serializable {

	private static final long serialVersionUID = 2370307087717576892L;

	private Integer total;
	private List<T> list;

}