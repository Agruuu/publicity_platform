package com.orhonit.publicity.common.event;

import com.orhonit.publicity.common.constants.EventType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * 自定义事件
 */
@Getter
@Setter
public class AdminEvent extends ApplicationEvent {

	private static final long serialVersionUID = -8244067001371123563L;

	private EventType eventType;

	public AdminEvent(Object source, EventType eventType) {
		super(source);
		this.eventType = eventType;
	}

}
