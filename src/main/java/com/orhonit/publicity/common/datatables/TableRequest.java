package com.orhonit.publicity.common.datatables;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

/**
 * 分页查询参数
 */
@Getter
@Setter
public class TableRequest implements Serializable {

	private static final long serialVersionUID = 5159229192412391644L;

	private Integer start;
	private Integer length;
	private Map<String, Object> params;
}
