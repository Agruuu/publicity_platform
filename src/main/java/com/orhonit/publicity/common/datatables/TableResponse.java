package com.orhonit.publicity.common.datatables;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 分页查询返回
 */
@Getter
@Setter
@Builder
public class TableResponse<T> implements Serializable {

	private static final long serialVersionUID = 620421858510718076L;

	private Integer recordsTotal;
	private Integer recordsFiltered;
	private List<T> data;

}